"""
    Python script for simulating the OCT signal to noise ratio. If you use this code, please refer to
    
    Fourier-domain optical coherence tomography signal analysis and numerical modeling
    J. Kalkman, International Journal of Optics 2017, 1 (2017).
"""

import numpy as np
import matplotlib.pyplot as plt

plt.close('all')

# OCT system parameters
lambdac=850e-9                  # center wavelength [m]
dlambda=50e-9                   # FWHM width of the source [m]
P0=1e-3                         # source power [W]
tau=1e-3                        # integration time [s]
alpha=0.5                       # intensity beam splitting ratio
M=1024                          # number of samples
rs=1e-2                         # amplitude reflection coefficient sample

k=np.linspace(1/(lambdac+dlambda/2), 1/(lambdac-dlambda/2),M)
deltak=k[1]-k[0]
z=np.linspace(-1/2/deltak, 1/2/deltak, M)##check

# Calculate number of photons
vuc=3e8/lambdac
Ntot=P0*tau/(6.6e-34*vuc)
Ns=alpha*(1-alpha)*Ntot*rs**2/M # number of detected photons per channel from the sample arm

# Simulation parameters
Q=1000                           # number of ensemble realizations
Nrefl=11                          # number of reference arm reflectivity values
rr=np.logspace(-4.5, 0, Nrefl)     # amplitude reflection coefficient reference
zdisc=64                         # mirror location (pixels)

data=np.zeros((Nrefl, 15))
m=np.linspace(0, M-1, M)
m=np.expand_dims(m, axis=1)
m=np.rot90(m)

dummy=np.ones((1,Q))            
dummy=np.rot90(dummy)

for cnt in np.arange(Nrefl):
    print('Reference reflectivity is ', rr[cnt])
       
    Nks=np.random.poisson(Ns, [Q, M])
    # For large N can be replaced with
    # Nks=Ns+np.sqrt(Ns)*np.random.randn(Q, M)
    
    Nr=alpha*(1-alpha)*Ntot*rr[cnt]**2/M # Nr is the number of photons per bin detected from the reference arm
    
    Nkr=np.random.poisson(Nr, [Q, M])
    # For large N can be replaced with
    # Nkr=Nr+np.sqrt(Nr)*np.random.randn(Q, M)
    
    # Calculate the fields                   
    Ekref=np.sqrt(Nkr)
    Eksam=np.sqrt(Nks)*np.exp(1j*2*np.pi*np.matmul(dummy,m)*zdisc/M)
    
    # Calculate terms
    interference=(Eksam+Ekref)*np.conj(Eksam+Ekref)
    sampleint=np.real(np.mean(Eksam*np.conj(Eksam), axis=0))
    refint=np.real(np.mean(Ekref*np.conj(Ekref), axis=0))
    
    # Interference spectrum
    Nkint= np.real(interference - sampleint - refint)
    
    # Complex OCT signal     
    iz=np.fft.fftshift(np.fft.ifft(Nkint, M, 1))
     
    # OCT intensity peak signal estimation
    peak=np.mean((np.abs(iz[:, int(M/2) - zdisc]))**2,0)
    
    noisefloor=np.mean((np.abs(iz[:, int(M/2)]))**2,0)
    data[cnt,0]=peak-noisefloor    # Peak OCT intensity        
    
    # model of OCT signal cf. Eq. 39, take sufficient number of M>512    
    data[cnt,1]=(Ntot*alpha*(1-alpha)*rr[cnt]*rs/M)**2 
         
    # Independent noise estimation, incoherent addition
    iznoise=np.fft.ifft(Nks + Nkr, M, 1)            # noise in the absence of the peak 
    
    # Plot a the Fourier domain signal
    if cnt==0:
        plt.figure(2)
        plt.title('OCT signal for rr= ' + str(rr[cnt]))
        plt.semilogy(1e3*z, np.mean(abs(iz[:,:])**2,0), '-b', label='OCT intensity')
        plt.axhline(y=peak, color='r', linestyle='--', label='OCT peak intensity')
        plt.axhline(y=noisefloor, color='r', linestyle='--', label='OCT intensity noise floor')
        plt.xlabel('Depth (mm)'), plt.ylabel('OCT intensity (arb. units)'), plt.grid(), plt.xlim([1e3*min(z), 1e3*max(z)]), plt.legend()

    data[cnt,2]=np.mean(np.std(np.abs(iznoise[:, M-100:M]**2), 1), 0)   # std noise only model
    # Estimating the noise in the full OCT model gives a slight offset
    # np.mean(np.std((np.abs(iz[:, M-100:M]))**2, 1),0)   # std full OCT model
    
    data[cnt,3]=Ntot*alpha*(1-alpha)*(rs**2+rr[cnt]**2)/(M**2) # variance noise model , cf. Eq. 42
    
    # SNR estimation
    data[cnt,4]=data[cnt,0]/data[cnt,2]; # SNR OCT simulation separate signal and noise
    data[cnt,5]=data[cnt,1]/data[cnt,3]; # theoretical SNR
        
    #  Calculate phase stability
    locationindex=int(M/2) - zdisc
    data[cnt,6]=np.var(np.arctan(np.imag(iz[:, locationindex])/np.real(iz[:, locationindex])))

plt.figure(1, figsize=[5,9])

plt.subplot(311)
plt.title('OCT signal')
plt.loglog(rr, data[:,1],'-b')
plt.loglog(rr, data[:,0],'or')     
plt.xlabel('Reference arm reflectivity'), plt.ylabel('OCT peak signal intensity (arb. units)')
plt.grid()

plt.subplot(312)
plt.title('OCT noise')
plt.loglog(rr, data[:,3],'-b', label='Model') 
plt.loglog(rr, data[:,2],'or', label='Noise only')    
plt.xlabel('Reference arm reflectivity'), plt.ylabel('Noise std (arb. units)')
plt.grid()
    
plt.subplot(313)
plt.title('OCT SNR')    
plt.loglog(rr, data[:,5],'-b')  
plt.loglog(rr, data[:,4],'or')
plt.xlabel('Reference arm reflectivity'), plt.ylabel('Signal to noise ratio'), plt.grid()

plt.tight_layout()

plt.figure(5, figsize=[4,4])
plt.title('OCT phase noise')
plt.loglog(data[:,5], 1/2/data[:,5],'-b', label='Theory') 
plt.loglog(data[:,4], data[:,6],'or', label='Simulation')
plt.xlabel('SNR'), plt.ylabel('Phase variance'), plt.grid(), plt.legend()