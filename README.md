# FD-OCT SNR simulator

This project provides a Python-based simulator of the SNR of a Fourier-domain OCT signal. 

The code was made with Python 3.7.3 and some basic output can be seen below. 

More information about the mathematics and physics behind this can be found in [1].

[1] _Fourier-domain optical coherence tomography signal analysis and numerical modeling_, J. Kalkman, International Journal of Optics 2017, 1 (2017)

<img src="fdoctsnrresults.png" alt="Output of the Python code" width="300"/>

